/*
 * hid.c -- HID Composite driver
 *
 * Based on multi.c
 *
 * Copyright (C) 2010 Fabien Chouteau <fabien.chouteau@barco.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */


#include <linux/kernel.h>
#include <linux/platform_device.h>
#include <linux/list.h>
#include <linux/module.h>
#include <linux/usb/composite.h>
#include <linux/usb/g_hid.h>

#define DRIVER_DESC		"HID Gadget"
#define DRIVER_VERSION		"2010/03/16"

#include "u_hid.h"

/*-------------------------------------------------------------------------*/

#define HIDG_VENDOR_NUM		0x0525	/* XXX NetChip */
#define HIDG_PRODUCT_NUM	0xa4ac	/* Linux-USB HID gadget */
#define TOUCH_REPORT_ID 0x01
#define PEN_REPORT_ID 0x02
#define MOUSE_REPORT_ID 0x03
#define DEVICE_MODE_FEATURE_ID 0x07
#define CONTACT_COUNT_FEATURE_ID 0x08

/*-------------------------------------------------------------------------*/

static struct hidg_func_descriptor my_hid_data = {
	.subclass		= 0, /* No subclass */
	.protocol		= 0, /* Keyboard */
	.report_length		= 64, //30
	.report_desc_length	= 292, //290
	.report_desc		= {
	0x05,0x0d, // USAGE_PAGE (Digitizers) 
		0x09,0x04, // USAGE (Touch Screen)
		0xa1,0x01, // COLLECTION (Application)
		0x85,TOUCH_REPORT_ID, //    REPORT_ID (Touch)    
		0x09,0x22, //    USAGE (Finger)
		0xa1,0x02, // 	    COLLECTION (Logical)
		0x15,0x00, //		   LOGICAL_MINIMUM (0)
		0x25,0x01, //          LOGICAL_MAXIMUM (1)
		0x09,0x42, //          USAGE (Tip Switch)
		0x75,0x01, //          REPORT_SIZE (1)
		0x95,0x01, //          REPORT_COUNT (1)
		0x81,0x02, //          INPUT (Data,Var,Abs)
		0x09,0x32, //          USAGE (In Range)
		0x81,0x02, //          INPUT (Data,Var,Abs)
		0x95,0x0e, //          REPORT_COUNT (14)
		0x81,0x03, //          INPUT (Data,Const,Abs)
		0x05,0x01, //          USAGE_PAGE (Generic Desktop) 
		0x75,0x10, //          REPORT_SIZE (16)
		0x95,0x01, //          REPORT_COUNT (1)
		0x26,0xff,0x7f,//      LOGICAL_MAXIMUM (32767)
		0x35,0x00, //          PHYSICAL_MINIMUM (0) 
		0x45,0x00, //          PHYSICAL_MAXIMUM (0)
		0x55,0x00,//           UNIT_EXPONENT (0)
		0x65,0x00,//           UNIT (NONE)
		0x09,0x30,//           USAGE (X)
		0x81,0x02,//           INPUT (Data,Var,Abs)
		0x09,0x31,//           USAGE (Y)
		0x81,0x02,//           INPUT (Data,Var,Abs)
		0x05,0x0d,//           USAGE_PAGE (Digitizers)
		0x09,0x30,//           USAGE (Tip Pressure)
		0x81,0x02,//           INPUT (Data,Var,Abs)
		0x09,0x48,//           USAGE (Width)   
		0x81,0x02,//           INPUT (Data,Var,Abs)
		0x09,0x49,//           USAGE (Height)
		0x81,0x02,//           INPUT (Data,Var,Abs)
		0x09,0x51,//           USAGE (Contact Identifier)
		0x81,0x02,//           INPUT (Data,Var,Abs)
		0xc0,//           END_COLLECTION
		0xa1,0x02,//        COLLECTION (Logical)
		0x15,0x00,//           LOGICAL_MINIMUM (0)
		0x25,0x01,//           LOGICAL_MINIMUM (1)
		0x09,0x42,//           USAGE (Tip Switch)
		0x75,0x01,//           REPORT_SIZE (1)
		0x95,0x01,//           REPORT_COUNT (1)
		0x81,0x02,//           INPUT (Data,Var,Abs)
		0x09,0x32,//           USAGE (In Range)
		0x81,0x02,//           INPUT (Data,Var,Abs)
		0x95,0x0e,//           REPORT_COUNT (14)
		0x81,0x03,//           INPUT (Const,Var,Abs)
		0x05,0x01,//           USAGE_PAGE (Generic Desktop)
		0x75,0x10,//           REPORT_SIZE (16)
		0x95,0x01,//           REPORT_COUNT (1)
		0x26,0xff,0x7f,//      LOGICAL_MAXIMUM (32767)
		0x35,0x00,//           PHYSICAL_MINIMUM (0)
		0x45,0x00,//           PHYSICAL_MAXIMUM (0)
		0x55,0x00,//           UNIT_EXPONENT (0)
		0x65,0x00,//           UNIT (NONE)
		0x09,0x30,//           USAGE (X)
		0x81,0x02,//           INPUT (Data,Var,Abs)
		0x09,0x31,//           USAGE (Y)
		0x81,0x02,//           INPUT (Data,Var,Abs)
		0x05,0x0d,//           USAGE_PAGE (Digitizers)
		0x09,0x30,//           USAGE (Tip Pressure)
		0x81,0x02,//           INPUT (Data,Var,Abs)
		0x09,0x48,//           USAGE (Width)   
		0x81,0x02,//           INPUT (Data,Var,Abs)
		0x09,0x49,//           USAGE (Height)
		0x81,0x02,//           INPUT (Data,Var,Abs)
		0x09,0x51,//           USAGE (Contact Identifier)
		0x81,0x02,//           INPUT (Data,Var,Abs)
		0xc0,//           END_COLLECTION

		0x05,0x0d,//        USAGE_PAGE (Digitizers) 
		0x09,0x54,//        USAGE (Contact Count)
		0x75,0x08,//        REPORT_SIZE (8)
		0x95,0x01,//        REPORT_COUNT (1)
		0x81,0x02,//        INPUT (Data,Var,Abs)

		0x06,0x00,0xff,//   USAGE_PAGE (Vendor Defined)
		0x09,0x01,//        USAGE (Vendor Usage 0x01)
		0x75,0x08,//        REPORT_SIZE (8)
		0x95,0x1D,//        REPORT_COUNT (29)
		0x91,0x02,//        OUTPUT (data, variable, absolute)

		0x85,CONTACT_COUNT_FEATURE_ID,//        Report_ID (08)
		0x05,0x0d,//        USAGE_PAGE (Digitizers)
		0x09,0x55,//        USAGE(Contact Count Maximum)
		0x25,0xff,//        LOGICAL_MAXIMUM (255)
		0x95,0x01,//        REPORT_COUNT (1)
		0x75,0x08,//        REPORT_SIZE (8)
		0xb1,0x02,//        FEATURE (Data,Var,Abs)
		0xc0,//     END_COLLECTION


		0x09,0x0e,//  USAGE (Device Configuration)
		0xa1,0x01,//  COLLECTION (Application)
		0x85,DEVICE_MODE_FEATURE_ID,//     Report_ID (Configuration)
		0x09,0x23,//     USAGE (Device Settings)
		0xa1,0x02,//     COLLECTION (Logical)
		0x09,0x52,//        USAGE (Device Mode)         
		0x09,0x53,//        USAGE (Device Identifier)
		0x15,0x00,//        LOGICAL_MINIMUM (0)
		0x25,0x0a,//        LOGICAL_MAXIMUM (10)
		0x75,0x08,//        REPORT_SIZE (8)
		0x95,0x02,//        REPORT_COUNT (2)
		0xb1,0x02,//        FEATURE (Data,Var,Abs)
		0xc0,//        END_COLLECTION
		0xc0,//      END_COLLECTION

	/*		 0x05, 0x01,                         // USAGE_PAGE (Generic Desktop)     0
			 0x09, 0x02,                         // USAGE (Mouse)                    2
			 0xa1, 0x01,                         // COLLECTION (Application)         4
			 0x85, MOUSE_REPORT_ID,              //   REPORT_ID (Mouse)              6
			 0x09, 0x01,                         //   USAGE (Pointer)                8
			 0xa1, 0x00,                         //   COLLECTION (Physical)          10
			 0x05, 0x09,                         //     USAGE_PAGE (Button)          12
			 0x19, 0x01,                         //     USAGE_MINIMUM (Button 1)     14
			 0x29, 0x02,                         //     USAGE_MAXIMUM (Button 2)     16
			 0x15, 0x00,                         //     LOGICAL_MINIMUM (0)          18
			 0x25, 0x01,                         //     LOGICAL_MAXIMUM (1)          20
			 0x75, 0x01,                         //     REPORT_SIZE (1)              22
			 0x95, 0x02,                         //     REPORT_COUNT (2)             24
			 0x81, 0x02,                         //     INPUT (Data,Var,Abs)         26
			 0x95, 0x06,                         //     REPORT_COUNT (6)             28
			 0x81, 0x03,                         //     INPUT (Cnst,Var,Abs)         30
			 0x05, 0x01,                         //     USAGE_PAGE (Generic Desktop) 32
			 0x09, 0x30,                         //     USAGE (X)                    34
			 0x09, 0x31,                         //     USAGE (Y)                    36
			 0x75, 0x10,                         //     REPORT_SIZE (16)             38
			 0x95, 0x02,                         //     REPORT_COUNT (2)             40
			 0x15, 0x00,                         //     LOGICAL_MINIMUM (0)          42
			 0x26, 0xff, 0x7f,                   //     LOGICAL_MAXIMUM (32767)      44
			 0x81, 0x02,                         //     INPUT (Data,Var,Abs)         47
			 0xc0,                               //   END_COLLECTION                 49
			 0xc0                                // END_COLLECTION                   50/51*/

// PEN REPORT
			0x05, 0x0d,                         // USAGE_PAGE (Digitizers)
			0x09, 0x02,                         // USAGE (Pen)
			0xa1, 0x01,                         // COLLECTION (Application)
			0x85, PEN_REPORT_ID,                 //  REPORT_ID (Pen)
			0x09, 0x20,                         //   USAGE (Stylus)
			0xa1, 0x00,                         //   COLLECTION (Logical)
			0x15, 0x00,                         //     LOGICAL_MINIMUM (0)
			0x25, 0x01,                         //     LOGICAL_MAXIMUM (1)
			0x09, 0x42,                         //     USAGE (Tip Switch)
			0x09, 0x32,                         //     USAGE (In Range)
			0x09, 0x44,                         //     USAGE (Barrel Switch)
			0x09, 0x45,                         //     USAGE (Eraser Switch)
			0x09, 0x3c,                         //     USAGE (Invert)
			0x75, 0x01,                         //     REPORT_SIZE (1)
			0x95, 0x05,                         //     REPORT_COUNT (5)
			0x81, 0x02,                         //     INPUT (Data,Var,Abs)
			0x95, 0x03,                         //     REPORT_COUNT (3)
			0x81, 0x03,                         //     INPUT (Cnst,Var,Abs)
			0x05, 0x01,                         //     USAGE_PAGE (Generic Desktop)
			0x75, 0x10,                         //     REPORT_SIZE (16)
			0x95, 0x01,                         //     REPORT_COUNT (1)
			0x26, 0xff, 0x7f,                   //     LOGICAL_MAXIMUM (32767)
			0x35, 0x00,                         //     PHYSICAL_MINIMUM (0)
			0x45, 0x00,                         //     PHYSICAL_MAXIMUM (0)
			0x55, 0x00,							//     UNIT_EXPONENT (0)
			0x65, 0x00,							//     UNIT (NONE)
			0x09, 0x30,                         //     USAGE (X)
			0x81, 0x02,                         //     INPUT (Data,Var,Abs)
			0x09, 0x31,                         //     USAGE (Y)
			0x81, 0x02,                         //     INPUT (Data,Var,Abs)
			0x05, 0x0d,                         //     USAGE_PAGE (Digitizers)
			0x09, 0x30,                         //     USAGE (Tip Pressure)
			0x81, 0x02,                         //     INPUT (Data,Var,Abs)
			0x09, 0x3d,                         //     USAGE (X Tilt)
			0x09, 0x3e,                         //     USAGE (Y Tilt)
			0x95, 0x02,                         //     REPORT_COUNT (2)
			0x81, 0x02,                         //     INPUT (Data,Var,Abs)

			0xc0,                               //   END_COLLECTION

			0x06, 0x00, 0xff,                   //     USAGE_PAGE (Vendor Defined)
			0x09, 0x02,                         //     USAGE (Vendor Usage 0x02)
			0x75, 0x08,                         //     REPORT_SIZE (8)
			0x95, 0x0B,                         //     REPORT_COUNT (11)
			0x91, 0x02,							//     OUTPUT (data, variable, absolute)


			0xc0                              // END_COLLECTION
	}
};

static struct platform_device my_hid = {
		.name			= "hidg",
		.id			= 0,
		.num_resources		= 0,
		.resource		= 0,
		.dev.platform_data	= &my_hid_data,
	};

struct hidg_func_node {
	struct usb_function_instance *fi;
	struct usb_function *f;
	struct list_head node;
	struct hidg_func_descriptor *func;
};

static LIST_HEAD(hidg_func_list);

/*-------------------------------------------------------------------------*/
USB_GADGET_COMPOSITE_OPTIONS();

static struct usb_device_descriptor device_desc = {
	.bLength =		sizeof device_desc,
	.bDescriptorType =	USB_DT_DEVICE,

	.bcdUSB =		cpu_to_le16(0x0200),

	/* .bDeviceClass =		USB_CLASS_COMM, */
	/* .bDeviceSubClass =	0, */
	/* .bDeviceProtocol =	0, */
	.bDeviceClass =		USB_CLASS_PER_INTERFACE,
	.bDeviceSubClass =	0,
	.bDeviceProtocol =	0,
	/* .bMaxPacketSize0 = f(hardware) */

	/* Vendor and product id can be overridden by module parameters.  */
	.idVendor =		cpu_to_le16(HIDG_VENDOR_NUM),
	.idProduct =		cpu_to_le16(HIDG_PRODUCT_NUM),
	/* .bcdDevice = f(hardware) */
	/* .iManufacturer = DYNAMIC */
	/* .iProduct = DYNAMIC */
	/* NO SERIAL NUMBER */
	.bNumConfigurations =	1,
};

static const struct usb_descriptor_header *otg_desc[2];

/* string IDs are assigned dynamically */
static struct usb_string strings_dev[] = {
	[USB_GADGET_MANUFACTURER_IDX].s = "",
	[USB_GADGET_PRODUCT_IDX].s = DRIVER_DESC,
	[USB_GADGET_SERIAL_IDX].s = "",
	{  } /* end of list */
};

static struct usb_gadget_strings stringtab_dev = {
	.language	= 0x0409,	/* en-us */
	.strings	= strings_dev,
};

static struct usb_gadget_strings *dev_strings[] = {
	&stringtab_dev,
	NULL,
};



/****************************** Configurations ******************************/

static int do_config(struct usb_configuration *c)
{
	struct hidg_func_node *e, *n;
	int status = 0;

	if (gadget_is_otg(c->cdev->gadget)) {
		c->descriptors = otg_desc;
		c->bmAttributes |= USB_CONFIG_ATT_WAKEUP;
	}

	list_for_each_entry(e, &hidg_func_list, node) {
		e->f = usb_get_function(e->fi);
		if (IS_ERR(e->f))
			goto put;
		status = usb_add_function(c, e->f);
		if (status < 0) {
			usb_put_function(e->f);
			goto put;
		}
	}

	return 0;
put:
	list_for_each_entry(n, &hidg_func_list, node) {
		if (n == e)
			break;
		usb_remove_function(c, n->f);
		usb_put_function(n->f);
	}
	return status;
}

static struct usb_configuration config_driver = {
	.label			= "HID Gadget",
	.bConfigurationValue	= 1,
	/* .iConfiguration = DYNAMIC */
	.bmAttributes		= USB_CONFIG_ATT_SELFPOWER,
};

/****************************** Gadget Bind ******************************/

static int hid_bind(struct usb_composite_dev *cdev)
{
	struct usb_gadget *gadget = cdev->gadget;
	struct list_head *tmp;
	struct hidg_func_node *n, *m;
	struct f_hid_opts *hid_opts;
	int status, funcs = 0;

	list_for_each(tmp, &hidg_func_list)
		funcs++;

	if (!funcs)
		return -ENODEV;

	list_for_each_entry(n, &hidg_func_list, node) {
		n->fi = usb_get_function_instance("hid");
		if (IS_ERR(n->fi)) {
			status = PTR_ERR(n->fi);
			goto put;
		}
		hid_opts = container_of(n->fi, struct f_hid_opts, func_inst);
		hid_opts->subclass = n->func->subclass;
		hid_opts->protocol = n->func->protocol;
		hid_opts->report_length = n->func->report_length;
		hid_opts->report_desc_length = n->func->report_desc_length;
		hid_opts->report_desc = n->func->report_desc;
	}


	/* Allocate string descriptor numbers ... note that string
	 * contents can be overridden by the composite_dev glue.
	 */

	status = usb_string_ids_tab(cdev, strings_dev);
	if (status < 0)
		goto put;
	device_desc.iManufacturer = strings_dev[USB_GADGET_MANUFACTURER_IDX].id;
	device_desc.iProduct = strings_dev[USB_GADGET_PRODUCT_IDX].id;

	if (gadget_is_otg(gadget) && !otg_desc[0]) {
		struct usb_descriptor_header *usb_desc;

		usb_desc = usb_otg_descriptor_alloc(gadget);
		if (!usb_desc)
			goto put;
		usb_otg_descriptor_init(gadget, usb_desc);
		otg_desc[0] = usb_desc;
		otg_desc[1] = NULL;
	}

	/* register our configuration */
	status = usb_add_config(cdev, &config_driver, do_config);
	if (status < 0)
		goto free_otg_desc;

	usb_composite_overwrite_options(cdev, &coverwrite);
	dev_info(&gadget->dev, DRIVER_DESC ", version: " DRIVER_VERSION "\n");

	return 0;

free_otg_desc:
	kfree(otg_desc[0]);
	otg_desc[0] = NULL;
put:
	list_for_each_entry(m, &hidg_func_list, node) {
		if (m == n)
			break;
		usb_put_function_instance(m->fi);
	}
	return status;
}

static int hid_unbind(struct usb_composite_dev *cdev)
{
	struct hidg_func_node *n;

	list_for_each_entry(n, &hidg_func_list, node) {
		usb_put_function(n->f);
		usb_put_function_instance(n->fi);
	}

	kfree(otg_desc[0]);
	otg_desc[0] = NULL;

	return 0;
}

static int hidg_plat_driver_probe(struct platform_device *pdev)
{
	struct hidg_func_descriptor *func = dev_get_platdata(&pdev->dev);
	struct hidg_func_node *entry;

	if (!func) {
		dev_err(&pdev->dev, "Platform data missing\n");
		return -ENODEV;
	}

	entry = kzalloc(sizeof(*entry), GFP_KERNEL);
	if (!entry)
		return -ENOMEM;

	entry->func = func;
	list_add_tail(&entry->node, &hidg_func_list);

	return 0;
}

static int hidg_plat_driver_remove(struct platform_device *pdev)
{
	struct hidg_func_node *e, *n;

	list_for_each_entry_safe(e, n, &hidg_func_list, node) {
		list_del(&e->node);
		kfree(e);
	}

	return 0;
}


/****************************** Some noise ******************************/


static struct usb_composite_driver hidg_driver = {
	.name		= "g_hid",
	.dev		= &device_desc,
	.strings	= dev_strings,
	.max_speed	= USB_SPEED_HIGH,
	.bind		= hid_bind,
	.unbind		= hid_unbind,
};

static struct platform_driver hidg_plat_driver = {
	.remove		= hidg_plat_driver_remove,
	.driver		= {
		.name	= "hidg",
	},
};


MODULE_DESCRIPTION(DRIVER_DESC);
MODULE_AUTHOR("Fabien Chouteau, Peter Korsgaard");
MODULE_LICENSE("GPL");

static int __init hidg_init(void)
{
	int status;
	status = platform_device_register(&my_hid);
	if (status < 0)
	{
		platform_driver_unregister(&my_hid);
		return status;
	}
	status = platform_driver_probe(&hidg_plat_driver,
				hidg_plat_driver_probe);
	if (status < 0)
		return status;

	status = usb_composite_probe(&hidg_driver);
	if (status < 0)
		platform_driver_unregister(&hidg_plat_driver);

	return status;
}
module_init(hidg_init);

static void __exit hidg_cleanup(void)
{
	usb_composite_unregister(&hidg_driver);
	platform_driver_unregister(&hidg_plat_driver);
	platform_device_unregister(&my_hid);
}
module_exit(hidg_cleanup);
