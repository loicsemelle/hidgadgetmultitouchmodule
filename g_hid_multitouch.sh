#!/bin/bash

sudo wget https://raw.githubusercontent.com/notro/rpi-source/master/rpi-source -O /usr/bin/rpi-source 

sudo chmod +x /usr/bin/rpi-source

/usr/bin/rpi-source -q --tag-update

rpi-source -d $HOME

FIRMWARE_HASH=$(zgrep "* firmware as of" /usr/share/doc/raspberrypi-bootloader/changelog.Debian.gz | head -1 | awk '{ print $5 }')

KERNEL_HASH=$(wget https://raw.github.com/raspberrypi/firmware/$FIRMWARE_HASH/extra/git_hash -O -)

sudo ln -s $HOME/linux-$KERNEL_HASH /lib/modules/$(uname -r)/build 

curl -L https://raw.githubusercontent.com/raspberrypi/firmware/$FIRMWARE_HASH/extra/Module.symvers > /lib/modules/$(uname -r)/build/Module.symvers

mkdir build

cd build

cp ../f_hid.c .
cp ../hid.c .
cp ../Makefile .

make clean

make

sudo rmmod g_hid

sudo rmmod usb_f_hid

sudo cp g_hid.ko /lib/modules/$(uname -r)/kernel/drivers/usb/gadget/legacy/

sudo cp usb_f_hid.ko /lib/modules/$(uname -r)/kernel/drivers/usb/gadget/function/

cd ..

sudo cp config.txt /boot

sudo cp cmdline.txt /boot




