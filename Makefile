ccflags-y := -I$(srctree)/drivers/usb/gadget/ 
ccflags-y += -I$(srctree)/drivers/usb/gadget/udc/ 
ccflags-y += -I$(srctree)/drivers/usb/gadget/function/

usb_f_hid-y	:= f_hid.o
g_hid-y	:= hid.o

obj-m += g_hid.o
obj-m	+= usb_f_hid.o

all:
	make -C /lib/modules/$(shell uname -r)/build M=$(PWD) modules

clean:
	make -C /lib/modules/$(shell uname -r)/build M=$(PWD) clean
